package com.anh.demo.repository;

import com.anh.demo.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUserRepository extends JpaRepository<User,Long> {
// viet query
}
