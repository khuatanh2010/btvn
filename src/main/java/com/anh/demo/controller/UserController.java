package com.anh.demo.controller;

import com.anh.demo.exception.ResourceNotFoundException;
import com.anh.demo.model.User;
import com.anh.demo.repository.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private IUserRepository iUserRepository;
    @PostMapping
    public User createUser(@RequestBody User user){
        return iUserRepository.save(user);
    }
    @GetMapping
    public List<User> getAllUser(){
       return iUserRepository.findAll();
    }
    @GetMapping("{id}")
    public ResponseEntity<User> getUSerById(@PathVariable Long id){
        User user = iUserRepository.findById(id).orElseThrow(()->new ResourceNotFoundException("User not found"));
        return ResponseEntity.ok(user);
    }
    @PutMapping("{id}")
    public ResponseEntity<User> updateUser(@PathVariable Long id,@RequestBody User userDetail){
        User updateUser = iUserRepository.findById(id).orElseThrow(()->new ResourceNotFoundException("User not found"));
        updateUser.setUsername(userDetail.getUsername());
        updateUser.setPassword(userDetail.getPassword());
        iUserRepository.save(updateUser);
        return ResponseEntity.ok(updateUser);
    }
    @DeleteMapping("{id}")
    public ResponseEntity<User> deleteUserById(@PathVariable Long id){
        User user = iUserRepository.findById(id).orElseThrow(()->new ResourceNotFoundException("User not found"));
        iUserRepository.delete(user);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
